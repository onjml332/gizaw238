#第一财经JJ打鱼商人uuv
#### 介绍
JJ打鱼商人【溦:844825】，JJ打鱼商人【溦:844825】　　我和妻子放弃了车站的卖点，把装满锅碗瓢盆的平车拉到了孩子学校门前的街道上。我知道，这对一个年幼的孩子，有些残酷，但是在这残酷之中，我的孩子会得到一份其他孩子所不能获得的面对痛苦现实的勇气和为改变这现实而应具有的百折不挠的精神。
一别经年，曾今的十足都已变成回顾，时间急遽而过，本觉得不妨将你化成最卑鄙的过客，却创造，你从来遁世在我的内心，摘不掉，忘不掉。大概，这终身，你即是我的执念。

#### 参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
#### 特技
使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
Gitee 官方博客 blog.gitee.com
你可以 https://gitee.com/explore 这个地址来了解 Gitee 上的优秀开源项目
GVP 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
Gitee 官方提供的使用手册 https://gitee.com/help
Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 https://gitee.com/gitee-stars/